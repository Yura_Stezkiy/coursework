﻿using System;
using System.Collections.Generic;

namespace CourseWork.CovidSimulation
{
    public class Cell
    {
        private readonly Grid grid;
        private Random rand = new Random();

        public Cell(Grid grid, int x, int y)
        {
            this.grid = grid;
            X = x;
            Y = y;
        }

        public State CurrentState { get; set; }
        public State NextState { get; set; }
        public Precaution Precaution { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        protected List<Cell> GetNeighbours()
        {
            return grid.GetSickNeighbours(this);
        }

        public virtual void BeginTick()
        {
            List<Cell> numberOfNeighbours = GetNeighbours();

            //if (CurrentState == State.Sick)
            //{
            //    if (numberOfNeighbours.Count < 2) NextState = State.Healthy;
            //    if (numberOfNeighbours.Count >= 2 && numberOfNeighbours.Count <= 3) NextState = State.Sick;
            //    if (numberOfNeighbours.Count > 3) NextState = State.Healthy;
            //}
            //else if (numberOfNeighbours.Count == 3)
            //{
            //    NextState = State.Sick;
            //}


            if (CurrentState == State.Sick)
            {
                NextState = State.Sick;
            }
            else if(CurrentState == State.Healthy || CurrentState == State.Default)
            {
                if (numberOfNeighbours.Count > 0)
                {
                    if (Precaution == Precaution.NoPrecaution)   //if no precaution - 90% risk
                    {
                        bool getSick = RollChanceToGetSick(90);
                        if(getSick == true)
                        {
                            NextState = State.Sick;
                        }
                        else
                        {
                            NextState = State.Healthy;
                        }
                    }
                    else if (Precaution == Precaution.MaskOn)    //if wearing mask - 70% risk
                    {
                        bool getSick = RollChanceToGetSick(70);
                        if (getSick == true)
                        {
                            NextState = State.Sick;
                        }
                        else
                        {
                            NextState = State.Healthy;
                        }
                    }
                    else if (Precaution == Precaution.SocialDistancing)   //if social distancing - 50% risk
                    {
                        bool getSick = RollChanceToGetSick(50);
                        if (getSick == true)
                        {
                            NextState = State.Sick;
                        }
                        else
                        {
                            NextState = State.Healthy;
                        }
                    }
                }
            }
        }

        public virtual void EndTick()
        {
            CurrentState = NextState;
            NextState = State.Default;
        }

        private bool RollChanceToGetSick(int percentage)
        {
            int roll = rand.Next(0, 101);
            if(roll <= percentage)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public enum State
    {
        Default,
        Sick,
        Healthy,
    }

    public enum Precaution
    {
        NoPrecaution,
        MaskOn,
        SocialDistancing
    }
}