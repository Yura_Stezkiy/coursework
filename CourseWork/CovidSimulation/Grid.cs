﻿using System;
using System.Collections.Generic;

namespace CourseWork.CovidSimulation
{
    public class Grid
    {
        private readonly int width;
        private readonly int height;
        protected readonly Cell[][] cells;
        private int cellNumber;
        public Grid(int width, int height)
        {
            this.width = width;
            this.height = height;
            cells = new Cell[width][];
            cellNumber = width * height;
        }

        public int Width
        {
            get { return width; }
        }

        public int Height
        {
            get { return height; }
        }
        public int CellNumber
        {
            get { return cellNumber; }
        }

        public void Initialise()
        {
            for (var widthCount = 0; widthCount < width; widthCount++)
            {
                cells[widthCount] = new Cell[height];

                for (var heightCount = 0; heightCount < height; heightCount++)
                {
                    cells[widthCount][heightCount] = new Cell(this, widthCount, heightCount);
                }
            }
        }

        public void Tick()
        {
            ActOnCells(c => c.BeginTick());
            ActOnCells(c => c.EndTick());
            MainWindow.iteration++;
        }

        private void ActOnCells(Action<Cell> cellAction)
        {
            for (var widthCount = 0; widthCount < width; widthCount++)
            {
                for (var heightCount = 0; heightCount < height; heightCount++)
                {
                    cellAction(cells[widthCount][heightCount]);
                }
            }
        }

        public virtual List<Cell> GetSickNeighbours(Cell cell)
        {
            var x = cell.X;
            var y = cell.Y;
            List<Cell> sickNeighbours = new List<Cell>();

            if (CellIsAlive(x - 1, y - 1))  // top left
            {
                sickNeighbours.Add(CellAt(x - 1, y - 1));
            } 
            if (CellIsAlive(x, y - 1))  // top middle
            {
                sickNeighbours.Add(CellAt(x, y - 1));
            }
            if (CellIsAlive(x + 1, y - 1))  // top right
            {
                sickNeighbours.Add(CellAt(x + 1, y - 1));
            }
            if (CellIsAlive(x - 1, y))  // middle left
            {
                sickNeighbours.Add(CellAt(x - 1, y));
            }
            if (CellIsAlive(x + 1, y))  // middle right
            {
                sickNeighbours.Add(CellAt(x + 1, y));
            }
            if (CellIsAlive(x - 1, y + 1))// bottom right
            {
                sickNeighbours.Add(CellAt(x - 1, y + 1));
            }
            if (CellIsAlive(x, y + 1))  // bottom middle
            {
                sickNeighbours.Add(CellAt(x, y + 1));
            }
            if (CellIsAlive(x + 1, y + 1))  // bottom left
            {
                sickNeighbours.Add(CellAt(x + 1, y + 1));
            }

            return sickNeighbours;
        }

        private bool CellIsAlive(int x, int y)
        {
            if (x < 0 || y < 0 || x > width - 1 || y > height - 1) return false;

            return CellAt(x, y).CurrentState == State.Sick;
        }

        public Cell CellAt(int x, int y)
        {
            return cells[x][y];
        }
    }
}