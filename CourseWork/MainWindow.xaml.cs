﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Collections.Generic;


namespace CourseWork
{
    public partial class MainWindow
    {
        //GOL - Game Of Life / CS - Covid Simulation
        //Green Cell - healthy / Red Cell - sick / Blue Cell - healthy, wearing a mask / Purple Cell - healthy, socially distancing /

        private GameOfLife.Grid grid_GOL = new GameOfLife.Grid(50, 50);
        private CovidSimulation.Grid grid_CS = new CovidSimulation.Grid(50, 50);
        private int cellNumber;
        private bool stopRaised;
        private bool healthyCellsAvailable = true;
        public static int iteration = 0;
        private int peopleWearingMasks;
        private int peopleSociallyDistancing;
        private List<CovidSimulation.Cell> RandomMaskWearers = new List<CovidSimulation.Cell>();
        private List<CovidSimulation.Cell> RandomSocialDistancing = new List<CovidSimulation.Cell>();
        private List<CovidSimulation.Cell> MaskWearerCells = new List<CovidSimulation.Cell>();
        private List<CovidSimulation.Cell> SocialDistanceCells = new List<CovidSimulation.Cell>();
        Random rand = new Random();

        public MainWindow()
        {
            InitializeComponent();
            grid_GOL.Initialise();
            grid_CS.Initialise();

            Loaded += WindowUpdated_View;
            SizeChanged += WindowUpdated_View;
            ChooseAutomata.SelectionChanged += ChangeAutomata;

            GiveMasksPercentage.TextChanged += GetPeopleWearingMasks;
            SociallyDistancePercentage.TextChanged += GetPeopleSociallyDistancing;

            GiveMasks.Click += SetPeopleWearingMasks;
            SociallyDistance.Click += SetPeopleSociallyDistanced;
            MakeCSItemsHidden();
        }
        #region Work with Maskers/Distancing
        public void GetPeopleWearingMasks(object sender, RoutedEventArgs e)
        {
            try
            {
                peopleWearingMasks = Convert.ToInt32(GiveMasksPercentage.Text);
            }
            catch(FormatException)
            {
                return;
            }
        }
        public void GetPeopleSociallyDistancing(object sender, RoutedEventArgs e)
        {
            try
            {
                peopleSociallyDistancing = Convert.ToInt32(SociallyDistancePercentage.Text);
            }
            catch (FormatException)
            {
                return;
            }
        }
        public void SetPeopleWearingMasks(object sender, RoutedEventArgs e)
        {
            int iter = 0;
            while (iter < CalculatePeople(peopleWearingMasks))
            {
                int x = rand.Next(grid_CS.Width);
                int y = rand.Next(grid_CS.Height);
                var cell = grid_CS.CellAt(x, y);
                if (!RandomMaskWearers.Contains(cell))
                {
                    RandomMaskWearers.Add(cell);
                    cell.Precaution = CovidSimulation.Precaution.MaskOn;
                    MaskWearerCells.Add(cell);
                    DrawCell_CS_Mask(x, y);
                    iter++;
                }
            }
        }
        public void SetPeopleSociallyDistanced(object sender, RoutedEventArgs e)
        {
            int iter = 0;
            while (iter < CalculatePeople(peopleSociallyDistancing))
            {
                int x = rand.Next(grid_CS.Width);
                int y = rand.Next(grid_CS.Height);
                var cell = grid_CS.CellAt(x, y);
                if (!RandomSocialDistancing.Contains(cell))
                {
                    RandomSocialDistancing.Add(cell);
                    cell.Precaution = CovidSimulation.Precaution.SocialDistancing;
                    SocialDistanceCells.Add(cell);
                    DrawCell_CS_Distance(x, y);
                    iter++;
                }
            }
        }      
        private int CalculatePeople(int percentage)
        {
            return Math.Abs((grid_CS.CellNumber / 100) * percentage);
        }
        #endregion

        #region Control View

        public void ChangeAutomata(object sender, RoutedEventArgs e)
        {
            ClearClick_View(sender, e);
            if (ChooseAutomata.SelectedIndex == 0) //GOL
            {
                iteration = 0;
                MakeCSItemsHidden();
                PlayingField.Background = Brushes.White;
                DrawCells_GOL();
            }
            else if (ChooseAutomata.SelectedIndex == 1) //CS
            {
                iteration = 0;
                MakeCSItemsVisible();
                PlayingField.Background = Brushes.Green;
                DrawCells_CS();
            }
        }
        public void MakeCSItemsVisible()
        {
            GiveMasksPercentage.Visibility = Visibility.Visible;
            GiveMasks.Visibility = Visibility.Visible;
            SociallyDistancePercentage.Visibility = Visibility.Visible;
            SociallyDistance.Visibility = Visibility.Visible;
            percent.Visibility = Visibility.Visible;
            percent2.Visibility = Visibility.Visible;
            IterationsLabel.Visibility = Visibility.Visible;
            IterationsNumber.Visibility = Visibility.Visible;
        }
        public void MakeCSItemsHidden()
        {
            GiveMasksPercentage.Visibility = Visibility.Hidden;
            GiveMasks.Visibility = Visibility.Hidden;
            SociallyDistancePercentage.Visibility = Visibility.Hidden;
            SociallyDistance.Visibility = Visibility.Hidden;
            percent.Visibility = Visibility.Hidden;
            percent2.Visibility = Visibility.Hidden;
            IterationsLabel.Visibility = Visibility.Hidden;
            IterationsNumber.Visibility = Visibility.Hidden;
        }
        void WindowUpdated_View(object sender, RoutedEventArgs e)
        {
            if (ChooseAutomata.SelectedIndex == 0)
            {
                DrawGrid_GOL();
            }
            else if (ChooseAutomata.SelectedIndex == 1)
            {
                DrawGrid_CS();
            }
        }
        private void TickClick_View(object sender, RoutedEventArgs e)
        {
            if(ChooseAutomata.SelectedIndex == 0)
            {
                TickClick_GOL(sender,e);
            }
            else if(ChooseAutomata.SelectedIndex == 1)
            {
                TickClick_CS(sender, e);
            }
        }
        private void StartClick_View(object sender, RoutedEventArgs e)
        {
            if (ChooseAutomata.SelectedIndex == 0)
            {
                StartClick_GOL(sender, e);
            }
            else if (ChooseAutomata.SelectedIndex == 1)
            {
                StartClick_CS(sender, e);
            }
        }
        private void StopClick_View(object sender, RoutedEventArgs e)
        {
            if (ChooseAutomata.SelectedIndex == 0)
            {
                StopClick_GOL(sender, e);
            }
            else if (ChooseAutomata.SelectedIndex == 1)
            {
                StopClick_CS(sender, e);
            }
        }
        private void ClearClick_View(object sender, RoutedEventArgs e)
        {
            if (ChooseAutomata.SelectedIndex == 0)
            {
                ClearClick_GOL(sender, e);
            }
            else if (ChooseAutomata.SelectedIndex == 1)
            {
                ClearClick_CS(sender, e);
            }
        }
        private void UserClick_View(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (ChooseAutomata.SelectedIndex == 0)
            {
                UserClick_GOL(sender, e);
            }
            else if (ChooseAutomata.SelectedIndex == 1)
            {
                UserClick_CS(sender, e);
            }
        }
#endregion

        #region Conway's Game Of Life
        public void DrawGrid_GOL()
        {
            DrawGridLines_GOL();
            DrawCells_GOL();
        }

        private void BeginDraw_GOL()
        {
            var worker = new BackgroundWorker();
            worker.DoWork += WaitInterval_GOL;
            worker.RunWorkerCompleted += IntervalComplete_GOL;
            worker.RunWorkerAsync();
        }

        private void IntervalComplete_GOL(object sender, RunWorkerCompletedEventArgs e)
        {
            grid_GOL.Tick();
            DrawGrid_GOL();

            if (stopRaised)
            {
                stopRaised = false;
                return;
            }

            BeginDraw_GOL();
        }

        private void DrawCells_GOL()
        {
            for (var columnCount = 0; columnCount < grid_GOL.Width; columnCount++)
            {
                for (var rowCount = 0; rowCount < grid_GOL.Height; rowCount++)
                {
                    if (grid_GOL.CellAt(columnCount, rowCount).CurrentState != GameOfLife.State.Alive) continue;

                    DrawCell_GOL(columnCount, rowCount);
                }
            }
        }

        private void DrawCell_GOL(int columnCount, int rowCount)
        {
            var cellRectangle = new Rectangle
            {
                Fill = Brushes.Black,
                Width = GetCellWidth_GOL(),
                Height = GetCellHeight_GOL(),
                Margin = new Thickness(columnCount * GetCellWidth_GOL(), rowCount * GetCellHeight_GOL(), 0, 0)
            };
            cellRectangle.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Unspecified);
            PlayingField.Children.Add(cellRectangle);
        }

        private void DrawGridLines_GOL()
        {
            PlayingField.Children.Clear();

            for (var widthCount = 0; widthCount < grid_GOL.Width; widthCount++)
            {
                DrawGridLine_GOL(widthCount * GetCellWidth_GOL(), Orientation.Horizontal);
            }

            for (var heightCount = 0; heightCount < grid_GOL.Height; heightCount++)
            {
                DrawGridLine_GOL(heightCount * GetCellHeight_GOL(), Orientation.Vertical);
            }
        }

        private void DrawGridLine_GOL(double position, Orientation orientation)
        {
            var line = new Line
            {
                Stroke = Brushes.WhiteSmoke,
                StrokeThickness = 1,
                X1 = orientation == Orientation.Horizontal ? position : 0,
                X2 = orientation == Orientation.Horizontal ? position : PlayingField.ActualWidth,
                Y1 = orientation == Orientation.Vertical ? position : 0,
                Y2 = orientation == Orientation.Vertical ? position : PlayingField.ActualHeight,
            };

            line.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
            PlayingField.Children.Add(line);
        }

        private void TickClick_GOL(object sender, RoutedEventArgs e)
        {
            grid_GOL.Tick();
            DrawGrid_GOL();
        }

        private void StartClick_GOL(object sender, RoutedEventArgs e)
        {
            Start.IsEnabled = false;
            Stop.IsEnabled = true;
            Tick.IsEnabled = false;
            BeginDraw_GOL();
        }

        private void StopClick_GOL(object sender, RoutedEventArgs e)
        {
            Start.IsEnabled = true;
            Stop.IsEnabled = false;
            Tick.IsEnabled = true;
            stopRaised = true;
        }

        private void UserClick_GOL(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pos = e.GetPosition(e.Source as IInputElement);
            var cell = grid_GOL.CellAt(
                Convert.ToInt32(Math.Floor(pos.X / GetCellWidth_GOL())),
                Convert.ToInt32(Math.Floor(pos.Y / GetCellHeight_GOL()))
                );

            cell.CurrentState = cell.CurrentState == GameOfLife.State.Alive ? GameOfLife.State.Dead : GameOfLife.State.Alive;
            DrawGrid_GOL();
        }

        private void ClearClick_GOL(object sender, RoutedEventArgs e)
        {
            grid_GOL.Initialise();
            DrawGrid_GOL();
        }

        void WindowUpdated_GOL(object sender, RoutedEventArgs e)
        {
            DrawGrid_GOL();
        }

        private static void WaitInterval_GOL(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(50);
        }

        private double GetCellWidth_GOL()
        {
            return PlayingField.ActualWidth / grid_GOL.Width;
        }

        private double GetCellHeight_GOL()
        {
            return PlayingField.ActualHeight / grid_GOL.Height;
        }
        #endregion

        #region Covid Simulation
        public void DrawGrid_CS()
        {
            DrawGridLines_CS();
            foreach(CovidSimulation.Cell cell in MaskWearerCells)
            {
                DrawCell_CS_Mask(cell.X, cell.Y);
            }
            foreach (CovidSimulation.Cell cell in SocialDistanceCells)
            {
                DrawCell_CS_Distance(cell.X, cell.Y);
            }
            DrawCells_CS();
            IterationsNumber.Content = iteration;
        }

        private void BeginDraw_CS()
        {
            var worker = new BackgroundWorker();
            worker.DoWork += WaitInterval_CS;
            worker.RunWorkerCompleted += IntervalComplete_CS;
            worker.RunWorkerAsync();
        }

        private void IntervalComplete_CS(object sender, RunWorkerCompletedEventArgs e)
        {
            grid_CS.Tick();
            DrawGrid_CS();

            if (stopRaised)
            {
                stopRaised = false;
                return;
            }

            BeginDraw_CS();
        }

        private void DrawCells_CS()
        {
            bool thereAreStillHealthyCells = false;
            for (var columnCount = 0; columnCount < grid_CS.Width; columnCount++)
            {
                for (var rowCount = 0; rowCount < grid_CS.Height; rowCount++)
                {
                    if (grid_CS.CellAt(columnCount, rowCount).CurrentState != CovidSimulation.State.Sick)
                    {
                        thereAreStillHealthyCells = true;
                        continue;
                    }


                    DrawCell_CS(columnCount, rowCount);
                }
            }
            if(thereAreStillHealthyCells == false)
            {
                Stop.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            }
        }

        private void DrawCell_CS(int columnCount, int rowCount)
        {
            var cellRectangle = new Rectangle
            {
                Fill = Brushes.Red,
                Width = GetCellWidth(),
                Height = GetCellHeight(),
                Margin = new Thickness(columnCount * GetCellWidth(), rowCount * GetCellHeight(), 0, 0)
            };
            cellRectangle.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Unspecified);
            PlayingField.Children.Add(cellRectangle);
        }
        private void DrawCell_CS_Mask(int columnCount, int rowCount)
        {
            var cellRectangle = new Rectangle
            {
                Fill = Brushes.Blue,
                Width = GetCellWidth(),
                Height = GetCellHeight(),
                Margin = new Thickness(columnCount * GetCellWidth(), rowCount * GetCellHeight(), 0, 0)
            };
            cellRectangle.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Unspecified);
            PlayingField.Children.Add(cellRectangle);
        }
        private void DrawCell_CS_Distance(int columnCount, int rowCount)
        {
            var cellRectangle = new Rectangle
            {
                Fill = Brushes.Purple,
                Width = GetCellWidth(),
                Height = GetCellHeight(),
                Margin = new Thickness(columnCount * GetCellWidth(), rowCount * GetCellHeight(), 0, 0)
            };
            cellRectangle.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Unspecified);
            PlayingField.Children.Add(cellRectangle);
        }

        private void DrawGridLines_CS()
        {
            PlayingField.Children.Clear();

            for (var widthCount = 0; widthCount < grid_CS.Width; widthCount++)
            {
                DrawGridLine_CS(widthCount * GetCellWidth(), Orientation.Horizontal);
            }

            for (var heightCount = 0; heightCount < grid_CS.Height; heightCount++)
            {
                DrawGridLine_CS(heightCount * GetCellHeight(), Orientation.Vertical);
            }
        }

        private void DrawGridLine_CS(double position, Orientation orientation)
        {
            var line = new Line
            {
                Stroke = Brushes.WhiteSmoke,
                StrokeThickness = 1,
                X1 = orientation == Orientation.Horizontal ? position : 0,
                X2 = orientation == Orientation.Horizontal ? position : PlayingField.ActualWidth,
                Y1 = orientation == Orientation.Vertical ? position : 0,
                Y2 = orientation == Orientation.Vertical ? position : PlayingField.ActualHeight,
            };

            line.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
            PlayingField.Children.Add(line);
        }

        private void TickClick_CS(object sender, RoutedEventArgs e)
        {
            grid_CS.Tick();
            DrawGrid_CS();
        }

        private void StartClick_CS(object sender, RoutedEventArgs e)
        {
            Start.IsEnabled = false;
            Stop.IsEnabled = true;
            Tick.IsEnabled = false;
            BeginDraw_CS();
        }

        private void StopClick_CS(object sender, RoutedEventArgs e)
        {
            Start.IsEnabled = true;
            Stop.IsEnabled = false;
            Tick.IsEnabled = true;
            stopRaised = true;
        }

        private void UserClick_CS(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pos = e.GetPosition(e.Source as IInputElement);
            var cell = grid_CS.CellAt(
                Convert.ToInt32(Math.Floor(pos.X / GetCellWidth())),
                Convert.ToInt32(Math.Floor(pos.Y / GetCellHeight()))
                );

            cell.CurrentState = cell.CurrentState == CovidSimulation.State.Sick ? CovidSimulation.State.Healthy : CovidSimulation.State.Sick;
            cell.Precaution = CovidSimulation.Precaution.NoPrecaution;
            DrawGrid_CS();
        }

        private void ClearClick_CS(object sender, RoutedEventArgs e)
        {
            healthyCellsAvailable = true;
            grid_CS.Initialise();
            MaskWearerCells.Clear();
            SocialDistanceCells.Clear();
            DrawGrid_CS();
            iteration = 0;
            IterationsNumber.Content = iteration;
        }

        void WindowUpdated_CS(object sender, RoutedEventArgs e)
        {
            DrawGrid_CS();
        }

        private static void WaitInterval_CS(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(300);
        }

        private double GetCellWidth()
        {
            return PlayingField.ActualWidth / grid_CS.Width;
        }

        private double GetCellHeight()
        {
            return PlayingField.ActualHeight / grid_CS.Height;
        }
        #endregion

    }



}

